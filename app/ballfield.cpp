#include "ecl/ecl.h"
#include "org_lisp_ecl_EmbeddedCommonLisp.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <android/log.h>
#include <wchar.h>

#include <string.h>
#include <jni.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#if ANDROID
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "native-activity", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "native-activity", __VA_ARGS__))
#define LOGV(...) ((void)__android_log_print(ANDROID_LOG_VERBOSE, "native-activity", __VA_ARGS__))
#else
#define LOGI(...)
#define LOGW(...)
#define LOGE(...)
#endif

#include "ecl_boot.h"

#ifdef __cplusplus
#define ECL_CPP_TAG "C"
#else
#define ECL_CPP_TAG
#endif

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_screenkeyboard.h>

// char LispStartup[] = "(load \"/data/data/org.lisp.ecl/app_resources/etc/init.lisp\")";

char LispStartup[] = "(let (result) (handler-case (load \"/data/data/org.lisp.ecl/app_resources/etc/init.lisp\") (error (c) (setf result (apply #'format nil (simple-condition-format-control c) (simple-condition-format-arguments c))))) (or result (progn (start-swank) \"<ok>\"))) ";

cl_object my_eval()
{
  cl_object result;
  char *my_str;
  
  result = si_safe_eval(3, c_string_to_object(LispStartup), ECL_NIL, OBJNULL);
  result = si_copy_to_simple_base_string(result); 
  my_str = (char*)result->base_string.self;
  LOGI(my_str, 1);
}

int main(int argc, char* argv[])
{

  char *ecl = "ecl"  ;

  char tmp[2048];
  char *lisp_dir = "/data/data/org.lisp.ecl/app_resources/";
  int fd;
  
    /* ecl_set_option(ECL_OPT_TRAP_SIGFPE, 0); */
  /* ecl_set_option(ECL_OPT_TRAP_SIGSEGV, 0); */
  /* ecl_set_option(ECL_OPT_TRAP_SIGINT, 0); */
  /* ecl_set_option(ECL_OPT_TRAP_SIGILL, 0); */
  /* ecl_set_option(ECL_OPT_TRAP_SIGBUS, 0); */
  /* ecl_set_option(ECL_OPT_TRAP_INTERRUPT_SIGNAL, 0); */
  /* ecl_set_option(ECL_OPT_SIGNAL_HANDLING_THREAD, 0); */
  /* ecl_set_option(ECL_OPT_INCREMENTAL_GC, 0); */
  
  LOGI("Setting the directories\n");
  snprintf(tmp, 2048, "%s/", lisp_dir);
  setenv("ROOT", tmp, 1);
  
  snprintf(tmp, 2048, "%s/lib/", lisp_dir);
  setenv("ECLDIR", tmp, 1);
  
  snprintf(tmp, 2048, "%s/etc/", lisp_dir);
  setenv("ETC", tmp, 1);
  
  snprintf(tmp, 2048, "%s/home/", lisp_dir);
  setenv("HOME", tmp, 1);

  cl_boot(1, &ecl);
  
  LOGI("installing bytecodes compiler\n");
  si_safe_eval(3, c_string_to_object("(si:install-bytecodes-compiler)"), ECL_NIL, OBJNULL);
  
  LOGI("writing some info to stdout\n");
  si_safe_eval
    (3, c_string_to_object
     ("(format t \"ECL_BOOT, features = ~A ~%\" *features*)"),
     ECL_NIL, OBJNULL);
  si_safe_eval
    (3, c_string_to_object
     ("(format t \"(truename SYS:): ~A)\" (truename \"SYS:\"))"),
     ECL_NIL, OBJNULL);

  LOGI("Redirecting I/O\n");
  
  snprintf(tmp, 2048, "%s/ecl_output", lisp_dir);
  mkfifo(tmp, 0664);
  /* mode is O_RDWR to prevent blocking. Linux-specific. */
  fd = open(tmp, O_RDWR | O_NONBLOCK);
  dup2(fd, 1);
  dup2(fd, 2);
  close(fd);
  
  snprintf(tmp, 2048, "%s/ecl_input", lisp_dir);
  mkfifo(tmp, 0664);
  fd = open(tmp, O_RDONLY | O_NONBLOCK);
  dup2(fd, 0);
  close(fd);

  LOGI("Embed: Loading init.lisp...\n");
  
  CL_CATCH_ALL_BEGIN(ecl_process_env())
    {
      LOGI("Embed: before step 1...\n");
      si_safe_eval(3, c_string_to_object("(setq *default-pathname-defaults* #p\"/data/data/org.lisp.ecl/app_resources/\")"), ECL_NIL, OBJNULL);
      LOGI("Embed: after step 1...\n");
      si_select_package(ecl_make_simple_base_string("CL-USER", 7));
      LOGI("Embed: after step 2...\n");
      my_eval();
      LOGI("Embed: after step 3...\n");
    } CL_CATCH_ALL_END;

  LOGI("Embed: After init.lisp finished (or failed).\n");
  return 0;
}
