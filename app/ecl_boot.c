#include <stdlib.h>
#include <ecl/ecl.h>
#if ANDROID
#include <android/log.h>
#endif

#if ANDROID
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "native-activity", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "native-activity", __VA_ARGS__))
#define LOGV(...) ((void)__android_log_print(ANDROID_LOG_VERBOSE, "native-activity", __VA_ARGS__))
#else
#define LOGI(...)
#define LOGW(...)
#define LOGE(...)
#endif

#include "ecl_boot.h"

#ifdef __cplusplus
#define ECL_CPP_TAG "C"
#else
#define ECL_CPP_TAG
#endif

extern void loadLispFromAssets(char* fn);

void
ecl_boot(const char *root_dir)
{
        char *ecl = "ecl";

        /* ecl_set_option(ECL_OPT_TRAP_SIGFPE, 0); */
        /* ecl_set_option(ECL_OPT_TRAP_SIGSEGV, 0); */
        /* ecl_set_option(ECL_OPT_TRAP_SIGINT, 0); */
        /* ecl_set_option(ECL_OPT_TRAP_SIGILL, 0); */
        /* ecl_set_option(ECL_OPT_TRAP_SIGBUS, 0); */
        /* ecl_set_option(ECL_OPT_TRAP_INTERRUPT_SIGNAL, 0); */
        /* ecl_set_option(ECL_OPT_SIGNAL_HANDLING_THREAD, 0); */
        /* ecl_set_option(ECL_OPT_INCREMENTAL_GC, 0); */
}

void ecl_toplevel(const char *home)
{

  return;
}
