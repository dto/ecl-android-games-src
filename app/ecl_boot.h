#ifndef _ECL_BOOT_H_
#define _ECL_BOOT_H_

void ecl_boot(const char *root_dir);
void ecl_init(const char *home);
void ecl_toplevel(const char *message);

#endif
