;;; -*- lisp -*-

(in-package #:lispbuilder-sdl-gfx-cffi)

;;#+win32(eval-when (:compile-toplevel :load-toplevel :execute)
;;  (pushnew (merge-pathnames "../bin/" (directory-namestring (or *load-truename* *default-pathname-defaults*)))
;;	   cffi:*foreign-library-directories*
;;	   :test #'equal))

#+windows(eval-when (:compile-toplevel :load-toplevel :execute)
           (pushnew sdl-gfx-bin:*dll-path*
                    cffi:*foreign-library-directories*
                    :test #'equal))

(cffi:define-foreign-library sdl-gfx
  (:darwin (:or "libsdl_gfx.dylib" (:framework "sdl_gfx")))
  (:windows "sdl_gfx.dll")
  (:unix (:or "libsdl_gfx"
	      "libsdl_gfx.so"
	      "libsdl_gfx.so.4"
	      "libsdl_gfx.so.5"
	      "libsdl_gfx.so.13"
	      "libsdl_gfx.so.13.0.0")))

(cffi:use-foreign-library sdl-gfx)
