#!/bin/sh
DIR=`pwd`

emacs -Q --batch \
      --eval "(progn
     (add-to-list 'load-path (expand-file-name \"~/src/org/lisp/\"))
     (add-to-list 'load-path (expand-file-name \"~/src/org/contrib/lisp/\" t))
     (require 'org)(require 'ob)(require 'ob-tangle)
     (require 'ob-sh) (require 'ob-lisp)
     (defun p:confirm-babel-evaluate (lang body) nil) 
     (setq org-confirm-babel-evaluate 'p:confirm-babel-evaluate)
     (mapc (lambda (file)
            (find-file (expand-file-name file \"$DIR\"))
            (org-babel-tangle)
            (kill-buffer)) '(\"project.org\")))" 2>&1 
