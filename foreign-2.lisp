 
(cffi:define-foreign-library opengl
  (:darwin (:framework "OpenGL"))
  (:windows "opengl32.dll" :convention :stdcall)
  (:unix (:or "libGLESv1_CM.so" "libGLES_android.so")))

(cffi:use-foreign-library opengl)

;; (cffi:use-foreign-library lispbuilder-sdl-cffi::sdl-glue)

(cffi:define-foreign-library sdl
  (:darwin (:or (:framework "sdl")
                (:default "libsdl")))
  (:windows "sdl.dll")
  (:unix (:or "libsdl-1.2.so.0.7.2"
	      "libsdl-1.2.so.0"
	      "libsdl-1.2.so"
	      "libsdl.so"
	      "libsdl")))

(cffi:use-foreign-library sdl)

(cffi:define-foreign-library sdl-image
    (:darwin (:framework "sdl_image"))
    (:windows (:or "sdl_image.dll" "sdl_image1.2.dll"))
    (:unix (:or "libsdl_image-1.2.so.0"
            "libsdl_image1.2"
            "libsdl_image.so")))
;; (cffi:define-foreign-library zlib
;;   (:windows (:or "zlib1.dll")))
;; (cffi:define-foreign-library libpng
;;   (:windows (:or "libpng15-15.dll" "libpng12-0.dll")))
;; (cffi:define-foreign-library libjpg
;;   (:windows (:or "libjpeg-8.dll" "jpeg.dll")))
;; (cffi:define-foreign-library libtiff
;;   (:windows (:or "libtiff-5.dll" "libtiff-3.dll")))
(cffi:use-foreign-library sdl-image)
;; (cffi:use-foreign-library zlib)
;; (cffi:use-foreign-library libpng)
;; (cffi:use-foreign-library libjpg)
;; (cffi:use-foreign-library libtiff)

(cffi:define-foreign-library sdl-gfx
  (:darwin (:or "libsdl_gfx.dylib" (:framework "sdl_gfx")))
  (:windows "sdl_gfx.dll")
  (:unix (:or "libsdl_gfx"
	      "libsdl_gfx.so"
	      "libsdl_gfx.so.4"
	      "libsdl_gfx.so.5"
	      "libsdl_gfx.so.13"
	      "libsdl_gfx.so.13.0.0")))

(cffi:use-foreign-library sdl-gfx)

;(cffi:use-foreign-library libfreetype-6)

(cffi:define-foreign-library sdl-ttf-glue
  (:darwin (:or "liblispbuilder-sdl-ttf-glue"
            "lispbuilder-sdl-ttf-glue"))
  (:windows (:or "liblispbuilder-sdl-ttf-glue.dll"
             "lispbuilder-sdl-ttf-glue.dll"))
  (:unix "liblispbuilder-sdl-ttf-glue.so")
  (t (:or)))

(cffi:use-foreign-library sdl-ttf-glue)

(cffi:define-foreign-library sdl-ttf
  (:darwin (:or (:framework "sdl_ttf")
            (:framework "libsdl_ttf-2.0")))
  (:windows (:or "sdl_ttf.dll"))
  (:unix (:or "libsdl_ttf.so" "libsdl_ttf2.0" "libsdl_ttf-2.0.so.0")))

(cffi:use-foreign-library sdl-ttf)

(cffi:define-foreign-library sdl-mixer
  (:darwin (:framework "sdl_mixer"))
  (:windows "sdl_mixer.dll")
  (:unix (:or "libsdl_mixer"
	      "libsdl_mixer.so"
	      "libsdl_mixer-1.2.so"
	      "libsdl_mixer-1.2.so.0")))
(cffi:use-foreign-library sdl-mixer)
;; (cffi:use-foreign-library mikmod)
;; (cffi:use-foreign-library ogg)
;; (cffi:use-foreign-library vorbis)
;; (cffi:use-foreign-library vorbisfile)
;; (cffi:use-foreign-library smpeg)

