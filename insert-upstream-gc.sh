cd ecl
cp /usr/share/libtool/config/ltmain.sh .
cd src
mv bdwgc bdwgc.old
git clone git://github.com/ivmai/bdwgc.git
cd bdwgc
cp /usr/share/libtool/config/ltmain.sh .
git clone git://github.com/ivmai/libatomic_ops.git
cd libatomic_ops
cp /usr/share/libtool/config/ltmain.sh .
cd ..
autoreconf -vif
automake --add-missing
cd ..


