cd ecl/src
mv libffi libffi.old

git clone https://github.com/libffi/libffi

# wget https://github.com/libffi/libffi/archive/v3.2.1.tar.gz
# tar xvzf v3.2.1.tar.gz
# mv libffi-3.2.1 libffi

cd libffi
cp /usr/share/libtool/config/ltmain.sh .
autoreconf -ivf
cd ..
