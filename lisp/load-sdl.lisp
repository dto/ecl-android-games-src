;; (asdf:load-system :cffi)

(setf cffi:*foreign-library-directories*
      (union cffi:*foreign-library-directories*
	     '(#P"/data/data/org.lisp.ecl/app_resources/")
	     :test #'equal))

(cffi:define-foreign-library (sdl-native-helpers :convention :stdcall)
  (:unix (:or "libsdl_native_helpers.so")))
(cffi:use-foreign-library sdl-native-helpers)
;;(cffi:load-foreign-library #P"/system/lib/libsdl_native_helpers.so")

(cffi:define-foreign-library (sdl :convention :stdcall)
  (:darwin (:or (:framework "sdl")
		(:default "libsdl")))
  (:unix (:or "libsdl-1.2.so.0.7.2"
	      "libsdl-1.2.so.0"
	      "libsdl-1.2.so"
	      "libsdl.so"
	      "libsdl")))
(cffi:use-foreign-library sdl)
;;(cffi:load-foreign-library #P"/system/lib/libsdl-1.2.so")

(cffi:define-foreign-library (sdl-mixer :convention :stdcall)
  (:darwin (:or (:framework "sdl_mixer")
		(:default "libsdl_mixer")))
  (:unix (:or "libsdl_mixer-1.2.so.0.7.2"
	      "libsdl_mixer-1.2.so.0"
	      "libsdl_mixer-1.2.so"
	      "libsdl_mixer-1.2.so.0.2.6" ;; eeebuntu?
	      "libsdl_mixer.so"
	      "libsdl_mixer")))
(cffi:use-foreign-library sdl-mixer)
;;(cffi:load-foreign-library #P"/system/lib/libsdl_mixer.so")

(cffi:define-foreign-library (sdl-gfx :convention :stdcall)
  (:darwin (:or (:framework "sdl_gfx")
		(:default "libsdl_gfx")))
  (:unix (:or "libsdl_gfx-1.2.so.0.7.2"
	      "libsdl_gfx-1.2.so.0"
	      "libsdl_gfx-1.2.so"
	      "libsdl_gfx.so.4"
	      "libsdl_gfx.so.13"
	      "libsdl_gfx.so"
	      "libsdl_gfx")))
(cffi:use-foreign-library sdl-gfx)
;;(cffi:load-foreign-library #P"/system/lib/libsdl_gfx.so")

(cffi:define-foreign-library (sdl-ttf  :convention :stdcall)
  (:darwin (:or (:framework "sdl_ttf")
		(:default "libsdl_ttf")))
  (:unix (:or "libsdl_ttf-1.2.so.0.7.2"
	      "libsdl_ttf-1.2.so.0"
	      "libsdl_ttf-1.2.so"
	      "libsdl_ttf.so.4"
	      "libsdl_ttf.so.13"
	      "libsdl_ttf.so"
	      "libsdl_ttf")))
(cffi:use-foreign-library sdl-ttf)
;;(cffi:load-foreign-library #P"/system/lib/libsdl_ttf.so")

(cffi:define-foreign-library sdl-ttf-glue
  (:unix "liblispbuilder-sdl-ttf-glue.so")
  (t (:or)))
(cffi:use-foreign-library sdl-ttf-glue)

(cffi:define-foreign-library (sdl-image :convention :stdcall)
  (:darwin (:or (:framework "sdl_image")
		(:default "libsdl_image")))
  (:unix (:or "libsdl_image-1.2.so.0.7.2"
	      "libsdl_image-1.2.so.0"
	      "libsdl_image-1.2.so.0.1.5" ;; eeebuntu?
	      "libsdl_image-1.2.so"
	      "libsdl_image.so"
	      "libsdl_image")))
(cffi:use-foreign-library sdl-image)
;;(cffi:load-foreign-library #P"/system/lib/libsdl_image.so")
